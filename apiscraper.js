const { JSDOM } = require("jsdom");
const readline = require("readline-sync"); // why do we need a library for something so simple
const fs = require("fs");

JSDOM.fromURL("https://www.mtxserv.fr/api/doc").then(dom => {
	const resources = dom.window["resources"].children;
	var ret = {};

	for (let iRes = 0; iRes < resources.length; iRes++ ) {
		let branchName = resources[iRes].children[1].textContent.toLowerCase().trim().replace(/\s/g, "_");
		ret[branchName] = {};

		let uriBranch = resources[iRes].children[2].children;
		for (let iURI = 0; iURI < uriBranch.length; iURI++) {
			let subBranchs = uriBranch[iURI].children[1].children[0].children[0].children;
			for (let iSub = 0; iSub < subBranchs.length; iSub++) {
				const headInfos = subBranchs[iSub].children[0];
				const contentInfos = subBranchs[iSub].children[1].children[1].children[0].children;

				let methodPath = headInfos.children[0];
				let summary = headInfos.children[1].children[0].textContent.trim();

				let commandName = "";
				while ( commandName === "" ) {
					commandName = readline.question('(' + branchName + ") Summary: " + summary + "\nName : ").trim();
					if ( ret[branchName][commandName] ) {
						console.error("This name already exists, choose something else.");
						commandName = "";
					}
				}

				let method = methodPath.children[0].textContent.trim();
				let uri = methodPath.children[1].textContent.trim().replace("/api/v1/","");

				ret[branchName][commandName] = {
					"summary": summary,
					"method": method,
					"uri": uri,
					"arguments": {},
					"opt-arguments": {}
				};

				for (let iCon = 0; iCon < contentInfos.length; iCon++) {
					if ( contentInfos[iCon].tagName === "H4" ) {
						if ( contentInfos[iCon].textContent === "Requirements" ) {
							const table = contentInfos[iCon+1].children[1].children;
							for (let iTab = 0; iTab < table.length; iTab++) {
								let argName = table[iTab].children[0].textContent.trim();
								let argType = table[iTab].children[2].textContent.trim();
								ret[branchName][commandName]["arguments"][argName] = argType;
							}
						}
						else if ( contentInfos[iCon].textContent === "Parameters" ) {
							const table = contentInfos[iCon+1].children[1].children;
							for (let iTab = 0; iTab < table.length; iTab++) {
								let argName = table[iTab].children[0].textContent.trim();
								let argType = table[iTab].children[1].textContent.trim();
								let argRequired = table[iTab].children[2].textContent.trim();
								if ( argRequired === "false" )
									ret[branchName][commandName]["opt-arguments"][argName] = argType;
								else
									ret[branchName][commandName]["arguments"][argName] = argType;
							}
						}
					}
				}

				if ( ret[branchName][commandName]["arguments"].length === 0 )
					delete ret[branchName][commandName]["arguments"];

				if ( ret[branchName][commandName]["opt-arguments"].length === 0 )
					delete ret[branchName][commandName]["opt-arguments"];
				
			}
		}
	}
	fs.writeFile("mtxapi.json", JSON.stringify(ret, null, "\t"), "utf8", (err) => {
		if (err) throw err;
	});
});
