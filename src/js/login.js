const useful = require("../useful.js");
const {ipcRenderer} = require("electron");

let search = document.getElementsByName("submit");
var logSubmit = search[0];
var regSubmit = search[1];
search = document.getElementsByName("username");
var logUsername = search[0];
var regUsername = search[1];
search = document.getElementsByName("password");
var logPassword = search[0];
var regPassword = search[1];
var regEmail = document.getElementsByName("mail")[0];
var card = document.getElementsByClassName("card")[0];

function switchCard() {
	card.classList.toggle("switched")
}

search = document.getElementsByClassName("switch");
search[0].addEventListener("click", switchCard);
search[1].addEventListener("click", switchCard);

logSubmit.addEventListener("click", () => {
	let http_options = {
		host: "www.mtxserv.fr",
		headers: {
			"User-Agent": "Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11",
			"Connection": "keep-alive"
		}
	};

	useful.http_req(http_options, "/login", (res, body) => {
		let pattern = new RegExp(/name="_csrf_token" value="(.+?)"/);
		let csrf_token = pattern.exec(body)[1];
		let setCookie = res.headers["set-cookie"];
		let cookie = "";

		pattern = new RegExp(/(mtxserv=.+?;)/);

		for (let i = 0; i < setCookie.length; i++) {
			let localCookie = pattern.exec(setCookie[i]);
			if ( localCookie[1] ) {
				cookie = localCookie[1];
				break;
			}
		}

		http_options["headers"]["Cookie"] = cookie;

		let data = "_username=" + logUsername.value + "&_password=" + logPassword.value + "&_remember_me=on&_submit=Connexion&_csrf_token=" + csrf_token;

		useful.http_req(http_options, "/login_check", data, (res, body) => {
			cookie = "";
			pattern = new RegExp(/(.+?=.+?;)/);
			let setCookie = res.headers["set-cookie"];

			for (let i = 0; i < setCookie.length; i++) {
				let localCookie = pattern.exec(setCookie[i]);
				cookie += localCookie[1] + " ";
			}
			cookie = cookie.slice(0, -1); // remove last character
			http_options["headers"]["Cookie"] = cookie;

			useful.http_req(http_options, "/mtxserv-api", (res, body) => {
				pattern = new RegExp(/<li><strong>\sClient ID:.+?<\/strong> (.+?)<\/li>/);
				let client_id = pattern.exec(body)[1];
				pattern = new RegExp(/<li><strong>\sClient Secret:<\/strong> (?:&nbsp;)+(.+?)<\/li>/);
				let client_secret = pattern.exec(body)[1];
				pattern = new RegExp(/<li><strong> Api Key:<\/strong> (?:&nbsp;)+(.+?)<\/li>/);
				let api_key = pattern.exec(body)[1];
				let data = "client_id=" + client_id + "&client_secret=" + client_secret + "&api_key=" + api_key + "&grant_type=https%3A%2F%2Fwww.mtxserv.fr%2Fgrants%2Fapi_key";

				useful.http_req(http_options, "/oauth/v2/token", data, (res, body) => {
					let result = JSON.parse(body);
					global.access_token = result["access_token"];
					// global.save( result["refresh_token"], result["expires_in"] );
					console.log(global.access_token);
					ipcRenderer.send("channel", "to-dashboard");
				});
			});
		});
	});
});

regSubmit.addEventListener("click", switchCard);