var sidebar = document.getElementsByTagName("nav")[0];
var strap = document.getElementById("strap");
var strap1 = document.getElementsByClassName("strap1")[0];
var strap2 = document.getElementsByClassName("strap2")[0];
var strap3 = document.getElementsByClassName("strap3")[0];

function switchSidebar() {
	if ( sidebar.style.transform === "translateX(-120px)" ) {
		sidebar.style.transform = "translateX(0px)";
		strap.style.transform = "translateX(80px)";
		strap1.style.transform = "translateY(19px) translateX(10px) rotateZ(45deg)";
		strap2.style.transform = "translateY(17px) translateX(10px) rotateZ(45deg)";
		strap3.style.transform = "translateY(15px) translateX(10px) rotateZ(-45deg)";
	} else {
		sidebar.style.transform = "translateX(-120px)";
		strap.style.transform = "translateX(120px)";
		strap1.style.transform = "translateY(7px) translateX(10px)";
		strap2.style.transform = "translateY(17px) translateX(10px)";
		strap3.style.transform = "translateY(27px) translateX(10px)";
	}
}

var menuButton = document.getElementById("strap");
menuButton.addEventListener("click", switchSidebar);
switchSidebar();
