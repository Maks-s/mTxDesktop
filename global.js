const fs = require("fs");

function load() {
	fs.readFile("persist.nfo", (err, data) => {
		if (err) return;
		let pattern = new RegExp(/(.+?)=(.+?);/g);
		let result = pattern.exec(data);
		console.log(result);
	});
}

function save(refresh_token) {
	let data = "refresh_token=" + refresh_token + ";access_token=" + g_access_token + ";";
	fs.writeFile("persist.nfo", data, (err) => {
		if (err) throw err;
	});
}

module.exports = {
	load: load,
	save: save
};