const fs = require("fs");
const useful = require("./useful.js");
const querystring = require("querystring");

var mtxapi = {};
var http_options = {
	host: "www.mtxserv.fr",
	headers: {
		"User-Agent": "Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11",
		"Connection": "keep-alive"
	}
};

fs.readFile("mtxapi.json", (err, data) => {
	if (err) throw err;
	let result;
	try {
		result = JSON.parse(data);
	} catch (e) {
		console.error("Error when loading mtxapi.json: ", e);
	}
	if (typeof result === "undefined")
		return;
	mtxapi = result;
});

function apiCall(branch, operation, arguments, callback) {
	if ( !mtxapi[branch] )
		throw "mTxAPIError: Unknown branch " + branch;
	if ( !mtxapi[branch][operation] )
		throw "mTxAPIError: Unknown operation " + operation + " for branch " + branch;
	if ( !global.access_token )
		throw "mTxAPIError: Global access_token isn't initialized";

	let apiFunc = mtxapi[branch][operation];
	if ( apiFunc["method"] === "GET" || apiFunc["method"] === "DELETE" ) {

		let parsedURI = apiFunc["uri"] + "?access_token=" + global.access_token;

		for ( arg in arguments ) {
			if ( !arguments.hasOwnProperty(arg) || !(apiFunc["arguments"][arg] || apiFunc["opt-arguments"][arg]) ) // skip arguments we don't need
				continue;

			parsedURI = parsedURI.replace( new RegExp("{" + arg + "}", 'g'), (newStr, offset) => {
				let argValue = querystring.escape(arguments[arg]);
				delete arguments[arg];
				return argValue;
			});
			if ( !arguments[arg] )
				continue;

			parsedURI += "&" + querystring.escape(arg) + "=" + querystring.escape(arguments[arg]);
		}

		if ( apiFunc["method"] === "GET" ) {
			useful.http_req(http_options, "/api/v1/" + parsedURI, callback);
		} else {
			useful.delete("/api/v1/" + parsedURI, callback);
		}
	}
	else if ( apiFunc["method"] === "POST" || apiFunc["method"] === "PUT" ) {

		let parsedURI = apiFunc["uri"];
		let data = "access_token=" + global.access_token;

		for ( arg in arguments ) {
			if ( !arguments.hasOwnProperty(arg) || !(apiFunc["arguments"][arg] || apiFunc["opt-arguments"][arg]) )
				continue;

			parsedURI = parsedURI.replace( new RegExp("{" + arg + "}", 'g'), (newStr, offset) => {
				let argValue = querystring.escape(arguments[arg]);
				delete arguments[arg];
				return argValue;
			});
			if ( !arguments[arg] )
				continue;

			data += "&" + querystring.escape(arg) + "=" + querystring.escape(arguments[arg]);
		}

		if ( apiFunc["method"] === "POST" ) {
			useful.http_req(http_options, "/api/v1/" + parsedURI, data, callback);
		} else {
			useful.put("/api/v1/" + parsedURI, data, callback);
		}
	}
}

module.exports = {
	call: apiCall
};

/*
invoices/ :
[
  {
    "id": 82685,
    "created_at": "2016-02-17T15:01:03+0100",
    "expire_at": "2018-12-06T17:20:47+0100",
    "expire_at_in_day": 212,
    "is_install": true,
    "offer_id": 192,
    "offer_name": "Gmod - 12 slots",
    "type_id": 1,
    "type_name": "Serveur de jeux",
    "address": "gmod21.mtxserv.fr:27060",
    "game": "garry-s-mod",
    "gsid": 32748,
    "picture": "\\/bundles\\/hostmemanagergame\\/img\\/game\\/garry-s-mod.png",
    "extendable": true,
    "is_owner": true,
    "offer_amount": 9,
    "offer_amount_day": 0.3
  }
]
invoices/{id}/offers :
["6","8","10","12","16","20","24","28","32","48","64","80","100","128"]

apicall("admin", "add", {
	id: 444,
	adminId: 7897
});
*/