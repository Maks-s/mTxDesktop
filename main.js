const electron = require("electron");
const { app, BrowserWindow, ipcMain } = electron;
global.access_token = "dummyAPIKEY";

var windows;

function createWindow() {
	windows = new BrowserWindow({
		width: 800,
		height: 600,
		show: false,
		title: "mTxDesktop"
	});

	//windows.setMenu(null);
	windows.loadURL("file:///src/dashboard.html");

	windows.once("ready-to-show", () => {
		windows.show();
	});

	windows.on("closed", function() {
		windows = null;
	});

	setTimeout(() => {
		windows.loadURL("file:///src/login.html");
	}, 3000);

	ipcMain.on("channel", (e, type) => {
		if ( type === "to-dashboard" ) {
			windows.loadURL("file:///src/dashboard.html");
		}
	});
}

app.on("ready", createWindow);

app.on("window-all-closed", function() {
	if (process.platform !== "darwin") {
		app.quit();
	}
});

app.on("activate", function() {
	if (windows === null)
		createWindow();
});