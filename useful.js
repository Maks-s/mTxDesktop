const http = require("https");

// http_req(http_options, urlPath[, data], callback)
function http_req(http_options, urlPath, callback, data="") {
	if ( typeof callback === "string" ) {
		let transition = callback;
		callback = data;
		data = transition;
	}
	if ( data == "" ) {
		http_options["headers"]["Content-Type"] = "text/html; charset=UTF-8";
		if ( http_options["headers"]["Content-Length"] )
			delete http_options["headers"]["Content-Length"];
		http_options["path"] = urlPath;
		http_options["method"] = "GET";
	} else {
		http_options["headers"]["Content-Length"] = data.length;
		http_options["headers"]["Content-Type"] = "application/x-www-form-urlencoded; charset=UTF-8";
		http_options["path"] = urlPath;
		http_options["method"] = "POST";
	}
	let request = http.request(http_options, (res) => {
		res.setEncoding("utf8");

		var body = "";

		res.on("data", (chunk) => {
			body += chunk;
		});

		res.on("end", () => {
			callback(res, body);
		});
	});
	if ( data != "" ) {
		request.write(data);
	}
	request.end();
}

function http_delete(urlPath, callback) {
	let http_options = {
		host: "www.mtxserv.fr",
		method: "DELETE",
		path: urlPath,
		headers: {
			"User-Agent": "Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11",
			"Connection": "close"
		}
	};

	let request = http.request(http_options, (res) => {
		res.setEncoding("utf8");

		var body = "";

		res.on("data", (chunk) => {
			body += chunk;
		});

		res.on("end", () => {
			callback(res, body);
		});
	});
	request.end();
}

function http_put(urlPath, data, callback) {
	if ( data == "" )
		return;

	let http_options = {
		host: "www.mtxserv.fr",
		method: "PUT",
		path: urlPath,
		headers: {
			"User-Agent": "Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11",
			"Connection": "close",
			"Content-length": data.length
		}
	};

	let request = http.request(http_options, (res) => {
		res.setEncoding("utf8");

		var body = "";

		res.on("data", (chunk) => {
			body += chunk;
		});

		res.on("end", () => {
			callback(res, body);
		});
	});
	request.write(data);
	request.end();
}

module.exports = {
	http_req: http_req,
	put: http_put,
	delete: http_delete
};